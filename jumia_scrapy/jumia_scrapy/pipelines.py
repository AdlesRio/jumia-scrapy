# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import sqlite3

class JumiaScrapyPipeline:

    def __init__(self):

        self.create_connection()
        self.create_table()

    def create_connection(self): 
        try:
            self.conn = sqlite3.connect("jumia_smartphones.db")
            self.curr = self.conn.cursor()
        except :
            print("Error connecting to database")
    def create_table(self):
        try:
            self.curr.execute("drop table if exists smartphones_tb")
            self.curr.execute("""create table smartphones_tb(
            product_id INTEGER PRIMARY KEY AUTOINCREMENT,
            product_title text,
            product_price text
            )""")
        except :
            print("Error creating table")
    def store_db(self, item):
        try:
            self.curr.execute("insert into smartphones_tb (product_title, product_price) values (?,?)", (
            item["product_title"][0],
            item["product_price"][0])
            )
            self.conn.commit()
        except :
            print("Error inserting item")
    

    def process_item(self, item, spider):
       
        self.store_db(item)
        return item
