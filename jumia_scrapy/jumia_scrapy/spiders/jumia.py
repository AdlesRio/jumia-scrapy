import scrapy
from ..items import JumiaScrapyItem

class JumiaSpider(scrapy.Spider):
    name = 'jumia'
    allowed_domains = ['jumia.dz']
    start_urls = ['https://www.jumia.dz/smartphones/?viewType=list']

    def parse(self, response):
        
        for article in response.css("a.core"):
            items = JumiaScrapyItem()
            product_title = article.css(".name::text").extract()
            product_price = article.css(".prc::text").extract()

            items["product_title"] = product_title
            items["product_price"] = product_price

            yield items
        
        next_page = response.css('a[aria-label*="Page suivante"]::attr(href)').get()
        if next_page is not None :
            yield response.follow(next_page, callback = self.parse)